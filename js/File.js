import { files, saveButton, updateButton, titleItem } from './data.js'

export class File{
    constructor(id, title, description){
        this.id = id;
        this.title = title;
        this.description = description;
        this.uploadElement = () => {
                files.push({id, title, description})
                localStorage.setItem('Files', JSON.stringify(files))
                localStorage.setItem('Counter', id+1)
        }   
    }
}

export function generateElement(id, title){
       var component = document.createElement('li')
       var deleteButton = document.createElement('span')
       component.classList.add('list-group-item', 'd-flex', 'justify-content-between')
       component.classList.add('list-group-item')
       component.innerHTML = `<i class="far fa-file-code"></i> ${id+1} - ${title}`
       component.style.cursor = 'pointer'
       deleteButton.classList.add('delete', 'badge-pill')
       deleteButton.innerHTML = '<i class="far fa-trash-alt"></i>'
       deleteButton.style.color = 'red'
       component.addEventListener('click', () => {
           location.href = '#?id=' + id
           titleItem.value = title
           tinymce.get("texteditor").setContent(files[id].description)
           saveButton.classList.add('disabled')
           updateButton.classList.remove('d-none')
       })
    //    deleteButton.addEventListener('click', () => {
    //         var getParam = window.location.href
    //         var [url, id] = getParam.split('=')
    //         id = parseInt(id)
    //         for(var i = 0; i < files.length; i++){
    //             if(files[i].id == id){
    //                 files[i] = {id: id, title: titleItem.value, description: tinymce.get("texteditor").getContent()}
    //             }
    //         }
    //         localStorage.setItem('Files', JSON.stringify(files))
    //         location.href = ''
    //     })
       component.addEventListener('mouseover', () => {
           component.style.backgroundColor = 'lightgrey'
       })
       component.addEventListener('mouseleave', () => {
        component.style.backgroundColor = 'white'
       })
       component.appendChild(deleteButton)
       lista.appendChild(component)
}